import { environment } from "src/environment/environment";

const formatter = new Intl.NumberFormat(environment.language, {
  style: 'currency',
  currency: environment.currency
});

export const formatToCurrencyString: Function = (input: number): string => {
  return formatter.format(input);
}
