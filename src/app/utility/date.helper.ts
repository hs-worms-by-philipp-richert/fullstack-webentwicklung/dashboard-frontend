import { environment } from "src/environment/environment";

const formatter = new Intl.DateTimeFormat(environment.language, {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric'
});

const formatToDefaultDate: Function = (input: Date): string => {
  return formatter.format(input);
}

export default {
  formatToDefaultDate
};
