import { NgModule } from '@angular/core';
import { NotFoundComponent } from './not-found.component';
import { NbButtonModule, NbCardModule } from '@nebular/theme';

@NgModule({
  imports: [
    NbButtonModule,
    NbCardModule
  ],
  declarations: [
    NotFoundComponent
  ]
})

export class NotFoundModule {
}
