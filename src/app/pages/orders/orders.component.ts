import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { orderApi } from 'src/app/api/ordersApi/orders.api';
import { ICustomer } from 'src/app/shared/interfaces/ICustomer';
import { INation } from 'src/app/shared/interfaces/INation';
import { IOrder } from 'src/app/shared/interfaces/IOrder';
import { PagedOrderList } from 'src/app/shared/models/pagedOrderList';
import { formatToCurrencyString } from 'src/app/utility/money.helper';

@Component({
  selector: 'ngx-orders',
  styleUrls: ['./orders.component.scss'],
  templateUrl: './orders.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class OrdersComponent implements OnInit {
  loading: boolean = false;

  paginationSelection: string[] = ['1'];
  paginationTake: number = 50;

  paginationAmountPages: number = 1;
  paginationPages: number[] = [];

  orderList: IOrder[] = [];
  allOrdersAmount: number = 0;

  constructor(private cd: ChangeDetectorRef, private toastrService: NbToastrService) {
  }

  async ngOnInit(): Promise<void> {
    await this.loadData();
  }

  async loadData(): Promise<void> {
    try {
      this.loading = true;
      const list: PagedOrderList = await orderApi.getPaginatedList(Number(this.paginationSelection), this.paginationTake);

      this.allOrdersAmount = list.totalLength;

      this.paginationAmountPages = Math.floor(list.totalLength / this.paginationTake);
      
      if (list.totalLength % this.paginationTake)
        this.paginationAmountPages++;

      this.orderList = list.orderList as IOrder[];

      this.paginationPages = this.createRange(this.paginationAmountPages);

      this.loading = false;

      this.cd.detectChanges();
    } catch (error) {
      console.error('Error fetching list of orders', error);
      orderApi.errorHandler(this.toastrService, 'Error', 'Could not fetch list of orders');
    }
  }

  getCustomerFromOrder(order: IOrder): ICustomer {
    return order.custkey as unknown as ICustomer;
  }

  getNationFromOrder(order: IOrder): INation {
    return (order.custkey as unknown as ICustomer).nationkey as unknown as INation;
  }

  callFormatCurrencyHelper(num: number): string {
    return formatToCurrencyString(num);
  }

  trimPriority(str: string): string {
    return str.split('-')[1];
  }

  statusNamePriority(str: string): string {
    switch (str.split('-')[0]) {
      case '1':
        return 'danger';
      case '2':
        return 'warning';
      case '3':
        return 'info';
      case '5':
        return 'success';
      case '4':
      default:
        return 'basic';
    }
  }

  statusNameOrderstatus(str: string): string {
    switch(str) {
      case 'O':
        return 'success';
      case 'F':
      case 'P':
      default:
        return 'warning';
    }
  }

  tooltipTextOrderstatus(str: string): string {
    switch(str) {
      case 'O':
        return 'Shipments on time';
      case 'F':
        return 'Shipments behind schedule';
      case 'P':
      default:
        return 'Shipments partially behind schedule';
    }
  }

  createRange(num: number): number[] {
    return Array.from(new Array(num).keys());
  }

  async updatePaginationSelectionValue(value: string[]): Promise<void> {
    this.paginationSelection = value;
    this.cd.markForCheck();
    await this.loadData();
  }
}
