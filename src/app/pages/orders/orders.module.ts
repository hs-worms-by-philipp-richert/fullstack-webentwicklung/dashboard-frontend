import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersComponent } from './orders.component';
import { NbButtonGroupModule, NbCardModule, NbListModule, NbSpinnerModule, NbTagModule, NbTooltipModule, NbUserModule } from '@nebular/theme';

@NgModule({
  imports: [
    CommonModule,
    NbButtonGroupModule,
    NbCardModule,
    NbListModule,
    NbUserModule,
    NbTagModule,
    NbSpinnerModule,
    NbTooltipModule
  ],
  declarations: [
    OrdersComponent
  ]
})

export class OrdersModule {
}
