import { NgModule } from '@angular/core';
import { NbIconModule, NbLayoutModule, NbMenuItem, NbMenuModule, NbSidebarModule } from '@nebular/theme';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { NotFoundModule } from './not-found/not-found.module';
import { OrdersModule } from './orders/orders.module';
import { CustomersModule } from './customers/customers.module';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    NbMenuModule,
    NbLayoutModule,
    NbSidebarModule,
    NotFoundModule,
    DashboardModule,
    OrdersModule,
    CustomersModule,
    NbIconModule
  ],
  declarations: [
    PagesComponent,
  ]
})

export class PagesModule {
}
