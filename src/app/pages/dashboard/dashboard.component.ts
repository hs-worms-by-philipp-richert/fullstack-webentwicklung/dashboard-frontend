import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { orderApi } from 'src/app/api/ordersApi/orders.api';
import { EChartsDataModel } from 'src/app/models/echartsDataModel';
import { ChartTuple } from 'src/app/shared/models/chartTuple';
import dateHelper from 'src/app/utility/date.helper';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html'
})

export class DashboardComponent implements OnInit {
  profitChartData?: EChartsDataModel;
  revenueChartData?: EChartsDataModel;

  selectedPeriodForProfit: string = 'month';
  selectedPeriodForRevenue: string = 'alltime';

  constructor(private toastrService: NbToastrService) {
  }

  async ngOnInit() {
    await this.getProfitChartData(this.selectedPeriodForProfit);
    await this.getRevenueChartData(this.selectedPeriodForRevenue);
  }

  async getProfitChartData(periodType: string) {
    try {
      const res = await orderApi.getProfitChartData(periodType);
      const oneDay = 24*60*60*1000;

      if (res.length === 0 || res?.[0] === null) {
        orderApi.errorHandler(this.toastrService, 'Warning', 'No profit-chart data for this time-span', 'warning');
        return;
      }

      let finalChartDataPoints: ChartTuple[] = [res[0]];

      for (let i = 1; i < res.length; i++) {
        const lastDate: Date = new Date(res[i-1].date);
        const diffMs = Math.abs(lastDate.getTime() - res[i].date);
        const diffDays = Math.floor(diffMs / oneDay) - 1;

        for (let j = 0; j < diffDays; j++) {
          lastDate.setDate(lastDate.getDate() + 1)
          
          finalChartDataPoints.push({
            date: lastDate.getTime(),
            value: 0
          });
        }

        finalChartDataPoints.push(res[i]);
      }

      const profitChartRawData: EChartsDataModel = {
        chartLabel: finalChartDataPoints.map<string>(obj => {
          const formattedDate: string = dateHelper.formatToDefaultDate(new Date(obj.date));
          return formattedDate;
        }),
        linesData: [
          finalChartDataPoints.map<number>(obj => {
            if (Number.isNaN(obj.value))
              return 0;

            return obj.value;
          })
        ]
      };

      if (periodType.toLocaleLowerCase() === 'alltime') {
        const profitChartSummedData: EChartsDataModel = this.transformDataSummedToYear(profitChartRawData);

        this.profitChartData = profitChartSummedData;
      } else {
        this.profitChartData = profitChartRawData;
      }
    } catch (error) {
      console.error('Error fetching chart data', error);
      orderApi.errorHandler(this.toastrService, 'Error', 'Could not fetch chart data');
    }
  }

  async getRevenueChartData(periodType: string) {
    try {
      const res = await orderApi.getRevenueChartData(periodType);
      const oneDay = 24*60*60*1000;

      if (res.length === 0)
        return;

      let finalChartDataPoints: ChartTuple[] = [res[0]];

      for (let i = 1; i < res.length; i++) {
        const lastDate: Date = new Date(res[i-1].date);
        const diffMs = Math.abs(lastDate.getTime() - res[i].date);
        const diffDays = Math.floor(diffMs / oneDay) - 1;

        for (let j = 0; j < diffDays; j++) {
          lastDate.setDate(lastDate.getDate() + 1)
          
          finalChartDataPoints.push({
            date: lastDate.getTime(),
            value: 0
          });
        }

        finalChartDataPoints.push(res[i]);
      }

      const revenueChartRawData: EChartsDataModel = {
        chartLabel: finalChartDataPoints.map<string>(obj => {
          const formattedDate: string = dateHelper.formatToDefaultDate(new Date(obj.date));
          return formattedDate;
        }),
        linesData: [
          finalChartDataPoints.map<number>(obj => {
            if (Number.isNaN(obj.value))
              return 0;

            return obj.value;
          })
        ]
      };

      if (periodType.toLocaleLowerCase() === 'alltime') {
        const revenueChartSummedData: EChartsDataModel = this.transformDataSummedToYear(revenueChartRawData);

        this.revenueChartData = revenueChartSummedData;
      } else {
        this.revenueChartData = revenueChartRawData;
      }

    } catch (error) {
      console.error('Error fetching chart data', error);
      orderApi.errorHandler(this.toastrService, 'Error', 'Could not fetch chart data');
    }
  }

  private transformDataSummedToYear(revenueChartRawData: EChartsDataModel): EChartsDataModel {
    const revenueChartSummedData: EChartsDataModel = {
      chartLabel: [],
      linesData: [[]]
    };
    const yearRegex: RegExp = /[0-9]{4}$/g;
    
    let iterationYear: string | undefined;
    for (let i = 0; i < revenueChartRawData.chartLabel.length; i++) {
      const dateItem = revenueChartRawData.chartLabel[i];

      if (iterationYear === dateItem.match(yearRegex)?.[0]) {
        revenueChartSummedData.linesData[0][revenueChartSummedData.linesData[0].length-1] += revenueChartRawData.linesData[0][i];
        
        let lastElement = revenueChartSummedData.linesData[0][revenueChartSummedData.linesData[0].length-1];
        revenueChartSummedData.linesData[0][revenueChartSummedData.linesData[0].length-1] = Number(lastElement.toFixed(2))
      } else {
        iterationYear = dateItem.match(yearRegex)?.[0] ?? '';

        revenueChartSummedData.chartLabel.push(iterationYear);
        revenueChartSummedData.linesData[0].push(revenueChartRawData.linesData[0][i]);
      }
    }

    return revenueChartSummedData;
  }
}
