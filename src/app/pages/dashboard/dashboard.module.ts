import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { NbButtonModule, NbCardModule, NbIconModule, NbListModule, NbSelectModule, NbTabsetModule, NbUserModule, NbTooltipModule, NbDatepickerModule } from '@nebular/theme';
import { RevenueForecastComponent, RevenueOverviewComponent, ProfitChartComponent, OrdersTodayComponent, RevenueForDateComponent, ProfitForDateComponent, RevenueChartComponent } from 'src/app/components/orders';
import { TopCustomersComponent } from 'src/app/components/customers';
import { MostOrdersNationComponent } from 'src/app/components/nations';
import { NgxEchartsModule } from 'ngx-echarts';

@NgModule({
  imports: [
    NbCardModule,
    NbIconModule,
    NbTabsetModule,
    NbListModule,
    NbUserModule,
    CommonModule,
    NbButtonModule,
    NbSelectModule,
    NbTooltipModule,
    NgxEchartsModule,
    NbDatepickerModule
  ],
  declarations: [
    DashboardComponent,
    RevenueOverviewComponent,
    RevenueForecastComponent,
    TopCustomersComponent,
    MostOrdersNationComponent,
    ProfitChartComponent,
    RevenueChartComponent,
    OrdersTodayComponent,
    ProfitForDateComponent,
    RevenueForDateComponent
  ]
})

export class DashboardModule {
}
