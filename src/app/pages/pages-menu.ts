import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'home-outline',
    link: '/dashboard',
    home: true
  },
  {
    title: 'Orders',
    icon: 'list-outline',
    link: '/orders',
    home: false
  },
  {
    title: 'Customers',
    icon: 'people-outline',
    link: '/customers',
    home: false
  }
];
