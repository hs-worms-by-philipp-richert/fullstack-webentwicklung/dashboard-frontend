import { ChangeDetectorRef, Component } from '@angular/core';
import { MenuIcon } from '../utility/menu-icon.enum';
import { NbMenuItem, NbSidebarService } from '@nebular/theme';
import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['./pages.component.scss'],
  templateUrl: './pages.component.html'
})

export class PagesComponent {
  menuItems: NbMenuItem[] = [];
  showMenu: boolean = false;
  icon: MenuIcon;

  constructor(private sidebarService: NbSidebarService, private cd: ChangeDetectorRef) {
    this.menuItems = MENU_ITEMS;

    this.icon = MenuIcon.MenuOpened;
  }

  toggleMenu() {
    this.sidebarService.toggle(true, 'menu-sidebar');
  }
  
  toggleMenuIcon(value: string) {
    if (value == 'expanded') {
      this.icon = MenuIcon.MenuOpened;
    } else {
      this.icon = MenuIcon.MenuClosed;
    }

    this.cd.detectChanges();
  }
}
