import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersComponent } from './customers.component';
import { NbButtonGroupModule, NbCardModule, NbIconModule, NbListModule, NbSpinnerModule, NbTagModule, NbUserModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

@NgModule({
  imports: [
    CommonModule,
    NbButtonGroupModule,
    NbCardModule,
    NbListModule,
    NbUserModule,
    NbTagModule,
    NbSpinnerModule,
    NbIconModule,
    
  ],
  declarations: [
    CustomersComponent
  ]
})

export class CustomersModule {
}
