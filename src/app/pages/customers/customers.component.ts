import { ChangeDetectionStrategy,ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { ICustomer } from 'src/app/shared/interfaces/ICustomer';
import { PagedCustomerList } from 'src/app/shared/models/pagedCustomerList';
import { customerApi } from 'src/app/api/customersApi/customer.api';

@Component({
  selector: 'ngx-customers',
  styleUrls: ['./customers.component.scss'],
  templateUrl: './customers.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class CustomersComponent implements OnInit{
  
  loading: boolean = false;
  paginationSelection: string[] = ['1'];
  
  paginationTake: number = 30;

  paginationAmoutPages:number = 1;
  paginationPages: number[] = [];

  customerList: ICustomer[] = [];
  allCustomersAmount: number = 0;
  
  constructor(private cd: ChangeDetectorRef,private toastrService: NbToastrService){};

   async ngOnInit(): Promise<void> {
     await this.loadData();
  }

  async loadData(): Promise<void> {
    try{
      this.loading = true;
      const list: PagedCustomerList = await customerApi.getPaginatedList(Number(this.paginationSelection), Number(this.paginationTake));

      this.allCustomersAmount = list.totalLength;

      this.paginationAmoutPages = Math.floor(list.totalLength / this.paginationTake);
      
      if (list.totalLength % this.paginationTake)
        this.paginationAmoutPages++;

      this.customerList = list.customerList as ICustomer[];  

      this.paginationPages = this.createRange(this.paginationAmoutPages);

      this.loading = false;

      this.cd.detectChanges();
    } catch(error) {
      console.error('Error while fetching list of customers', error);
      customerApi.errorHandler(this.toastrService, 'Error', 'Could not fetch list of customers');
    }
  }

  createRange(num: number): number[] {
    return Array.from(new Array(num).keys());
  }

  async updatePaginationSelectionValue(value: string[]): Promise<void> {
    this.paginationSelection = value;
    this.cd.markForCheck()
    await this.loadData();
  }

}
