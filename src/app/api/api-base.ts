import { Inject, Injectable } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import axios, { Axios, AxiosInstance } from 'axios';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiBase {
  public axiosInstance: AxiosInstance;

  constructor(@Inject(String) endpoint: string) {
    this.axiosInstance = axios.create({
      baseURL: `${environment.baseApiHost}${endpoint}`,
      timeout: 15000
    });
  }

  errorHandler(toastrService: NbToastrService, title: string, message: string, status: string = 'danger'): void {
    toastrService.show(message, title, { status });
  }
}
