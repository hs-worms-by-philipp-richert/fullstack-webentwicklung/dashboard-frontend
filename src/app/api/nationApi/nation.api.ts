import { NationTopOrderModel } from 'src/app/shared/models/nationTopOrderModel';
import { ApiBase } from '../api-base';
import { AxiosResponse } from 'axios';

class NationApi extends ApiBase {
  constructor() {
    super('/nation');
  }

  async getMostOrdersNation(): Promise<NationTopOrderModel> {
    const res: AxiosResponse = await this.axiosInstance.get('/top-most-orders');

    return res.data as NationTopOrderModel;
  }

}

export const nationApi = new NationApi();
