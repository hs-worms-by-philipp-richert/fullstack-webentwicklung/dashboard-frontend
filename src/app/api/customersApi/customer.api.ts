import { CustomerRevenueModel } from 'src/app/shared/models/customerRevenueModel';
import { CustomerOrderCountModel } from 'src/app/shared/models/customerOrderCountModel';
import { ApiBase } from '../api-base';
import { AxiosResponse } from 'axios';
import { PagedCustomerList } from 'src/app/shared/models/pagedCustomerList';

class CustomerApi extends ApiBase {
  constructor() {
    super('/customer');
  }

  async getCustomersWithMostRevenue(): Promise<CustomerRevenueModel[]> {
    const res: AxiosResponse = await this.axiosInstance.get('/top-revenue/3');

    return res.data as CustomerRevenueModel[];
  }

  async getCustomersWithMostOrders(): Promise<CustomerOrderCountModel[]> {
    const res: AxiosResponse = await this.axiosInstance.get('/top-orders/5');

    return res.data as CustomerOrderCountModel[];
  }

  async getPaginatedList(page: number, take: number): Promise<PagedCustomerList> {
    const res: AxiosResponse = await this.axiosInstance.get(`list?page=${page}&take=${take}`);

    return res.data as PagedCustomerList;
  }

}

export const customerApi = new CustomerApi();
