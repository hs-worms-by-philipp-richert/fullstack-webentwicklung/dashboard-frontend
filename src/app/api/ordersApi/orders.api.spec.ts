import * as moxios from 'moxios';
import { orderApi } from './orders.api';
import { AlltimeRevenue, LastMonthRevenue, LastYearRevenue } from 'src/app/shared/models/revenueTypes';
import { OrdersCount } from 'src/app/shared/models/orderTypes';

describe('order api class', () => {
  beforeEach(() => {
    moxios.install(orderApi.axiosInstance);
  });

  afterEach(() => {
    moxios.uninstall(orderApi.axiosInstance);
  });

  it('should be created', () => {
    expect(orderApi).toBeTruthy();
  });

  it('returns value for revenue of last month', async () => {
    const lastMonthRevenueResponse: LastMonthRevenue = {
      lastMonthRevenue: 10000
    };

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: lastMonthRevenueResponse
      });
    });

    const result = await orderApi.getLastMonthRevenue();
    expect(result).toEqual(lastMonthRevenueResponse);
  });

  it('returns value for revenue of last year', async () => {
    const lastYearRevenueResponse: LastYearRevenue = {
      lastYearRevenue: 1200000
    };

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: lastYearRevenueResponse
      });
    });

    const result = await orderApi.getLastYearRevenue();
    expect(result).toEqual(lastYearRevenueResponse);
  });

  it('returns value for revenue of all-time', async () => {
    const allTimeRevenueResponse: AlltimeRevenue = {
      alltimeRevenue: 1000000
    };

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: allTimeRevenueResponse
      });
    });
    
    const result = await orderApi.getAllTimeRevenue();
    expect(result).toEqual(allTimeRevenueResponse);
  });

  it('returns value for orders of today', async () => {
    const ordersCountResponse: OrdersCount = {
      ordersCount: 69
    };
    
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: ordersCountResponse
      });
    });

    const result = await orderApi.getCountOfOrdersCurrentDay();
    expect(result).toEqual(ordersCountResponse);
  });
});
