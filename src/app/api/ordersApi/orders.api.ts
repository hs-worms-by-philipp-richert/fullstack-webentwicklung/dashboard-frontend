import { ChartTuple } from 'src/app/shared/models/chartTuple';
import { ApiBase } from '../api-base';
import { AxiosResponse } from 'axios';
import { AlltimeRevenue, ForecastNextYearRevenue, GenericRevenue, LastMonthRevenue, LastYearRevenue } from 'src/app/shared/models/revenueTypes';
import { PagedOrderList } from 'src/app/shared/models/pagedOrderList';
import { OrdersCount } from 'src/app/shared/models/orderTypes';

class OrderApi extends ApiBase {
  constructor() {
    super('/orders');
  }

  async getPaginatedList(page: number, take: number): Promise<PagedOrderList> {
    const res: AxiosResponse = await this.axiosInstance.get(`list?page=${page}&take=${take}`);

    return res.data as PagedOrderList;
  }

  async getLastMonthRevenue(): Promise<LastMonthRevenue> {
    const res: AxiosResponse = await this.axiosInstance.get('revenue/lastmonth');

    return res.data as LastMonthRevenue;
  }

  async getProfitChartData(periodType: string): Promise<ChartTuple[]> {
    const res: AxiosResponse = await this.axiosInstance.get(`/profit/chart-data/${periodType}`);

    return res.data.chartData as ChartTuple[];
  }

  async getRevenueChartData(periodType: string): Promise<ChartTuple[]> {
    const res: AxiosResponse = await this.axiosInstance.get(`/revenue/chart-data/${periodType}`);

    return res.data.chartData as ChartTuple[];
  }

  async getLastYearRevenue(): Promise<LastYearRevenue> {
    const res: AxiosResponse = await this.axiosInstance.get('revenue/lastyear');

    return res.data as LastYearRevenue;
  }

  async getAllTimeRevenue(): Promise<AlltimeRevenue> {
    const res: AxiosResponse = await this.axiosInstance.get('revenue/alltime');

    return res.data as AlltimeRevenue;
  }
  
  async getRevenueForecastNextYear(): Promise<ForecastNextYearRevenue> {
    const res: AxiosResponse = await this.axiosInstance.get('revenue/forecast/nextyear');

    return res.data as ForecastNextYearRevenue;
  }

  async getCountOfOrdersCurrentDay(): Promise<OrdersCount> {
    const res: AxiosResponse = await this.axiosInstance.get('count/today');

    return res.data as OrdersCount;
  }

  async getProfitOnDate(selectedDate: Date): Promise<GenericRevenue> {
    const day = selectedDate.getDate();
    const month = selectedDate.getMonth() + 1;
    const year = selectedDate.getFullYear();

    const res: AxiosResponse = await this.axiosInstance.get(`profit/on/${year}-${month}-${day}`);

    return res.data as GenericRevenue;
  }

  async getRevenueOnDate(selectedDate: Date): Promise<GenericRevenue> {
    const day = selectedDate.getDate();
    const month = selectedDate.getMonth() + 1;
    const year = selectedDate.getFullYear();

    const res: AxiosResponse = await this.axiosInstance.get(`revenue/on/${year}-${month}-${day}`);

    return res.data as GenericRevenue;
  }
}

export const orderApi = new OrderApi();
