export interface EChartsDataModel {
  chartLabel: string[],
  linesData: number[][]
}
