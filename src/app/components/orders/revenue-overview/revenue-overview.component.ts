import { Component, OnInit } from '@angular/core';
import { orderApi } from '../../../api/ordersApi/orders.api';
import { formatToCurrencyString } from 'src/app/utility/money.helper';
import { NbToastrService } from '@nebular/theme';
import { AlltimeRevenue, LastMonthRevenue, LastYearRevenue } from 'src/app/shared/models/revenueTypes';

@Component({
  selector: 'ngx-revenue-overview',
  templateUrl: './revenue-overview.component.html',
  styleUrls: ['./revenue-overview.component.scss']
}) 
export class RevenueOverviewComponent implements OnInit {

  revenue: string;
  selectedTime: string;

  constructor(private toastrService?: NbToastrService) {
    this.revenue = formatToCurrencyString(0.00);
    this.selectedTime = 'lastmonth';
  }

  async ngOnInit() {
    await this.getLastMonthRevenue();
  }

  async getData(timeParam: string) {
    switch (timeParam) {
      case 'lastmonth':
        await this.getLastMonthRevenue();
        break;
      case 'lastyear':
        await this.getLastYearRevenue();
        break;
      case 'alltime':
        await this.getAllTimeRevenue();
        break;
    }
  }

  async getLastMonthRevenue() {
    try {
      const res: LastMonthRevenue = await orderApi.getLastMonthRevenue();

      this.revenue = formatToCurrencyString(res.lastMonthRevenue);
    } catch (error) {
      console.error('Error fetching last month revenue', error);
      if (this.toastrService) orderApi.errorHandler(this.toastrService, 'Error', 'Could not fetch last month revenue');
    }
  }
  
  async getLastYearRevenue() {
    try {
      const res: LastYearRevenue = await orderApi.getLastYearRevenue();

      this.revenue = formatToCurrencyString(res.lastYearRevenue);
    } catch (error) {
      console.error('Error fetching last year revenue', error);
      if (this.toastrService) orderApi.errorHandler(this.toastrService, 'Error', 'Could not fetch last year revenue');
    }
  }

  async getAllTimeRevenue() {
    try {
      const res: AlltimeRevenue = await orderApi.getAllTimeRevenue();

      this.revenue = formatToCurrencyString(res.alltimeRevenue);
    } catch (error) {
      console.error('Error fetching last month revenue', error);
      if (this.toastrService) orderApi.errorHandler(this.toastrService, 'Error', 'Could not fetch all time revenue');
    }
  }
}
