import { Component, OnInit } from '@angular/core';
import { orderApi } from '../../../api/ordersApi/orders.api';
import { NbToastrService } from '@nebular/theme';
import { OrdersCount } from 'src/app/shared/models/orderTypes';

@Component({
  selector: 'ngx-orders-today',
  templateUrl: './orders-today.component.html',
  styleUrls: ['./orders-today.component.scss']
}) 
export class OrdersTodayComponent implements OnInit {
  constructor(private toastrService: NbToastrService) {
  }

  orderstoday: number = 0;
  
  async ngOnInit() {
    await this.getCountOfOrdersCurrentDay();
  }

  async getCountOfOrdersCurrentDay() {
    try {
      const res: OrdersCount = await orderApi.getCountOfOrdersCurrentDay();

      this.orderstoday = (res.ordersCount);
    } catch (error) {
      console.error('Error fetching count of orders current day', error);
      orderApi.errorHandler(this.toastrService, 'Error', 'Could not fetch today\'s count of orders');
    }
  }
}
