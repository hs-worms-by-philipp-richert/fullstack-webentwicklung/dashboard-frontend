import { Component, AfterViewInit, OnChanges, Input } from "@angular/core";
import type { EChartsOption, SeriesOption } from 'echarts';
import { NbJSThemeOptions, DEFAULT_THEME as baseTheme } from '@nebular/theme';
import * as echarts from 'echarts';

@Component({
  selector: 'ngx-profit-chart',
  styleUrls: ['./profit-chart.component.scss'],
  template: `<div echarts [options]="options" [merge]="options" class="echart" (chartInit)="onChartInit($event)"></div>`
})
export class ProfitChartComponent implements AfterViewInit, OnChanges {

  @Input()
  profitChartData?: { chartLabel: string[], linesData: (number | null)[][] };

  echartsInstance: any;
  options: EChartsOption = {};

  ngOnChanges(): void {
    if (this.options && this.profitChartData) {
      this.updateChartOptions(this.profitChartData);
    }
  }

  onChartInit(echarts: any) {
    this.echartsInstance = echarts;
  }

  ngAfterViewInit(): void {
    this.setOptions();

    if (this.profitChartData)
      this.updateChartOptions(this.profitChartData);
  }

  setOptions() {
    this.options = {
      grid: {
        left: 95,
        top: 20,
        right: 0,
        bottom: 40,
      },
      tooltip: {
        trigger: 'item',
        axisPointer: {
          type: 'cross',
          lineStyle: {
            color: 'rgba(0, 0, 0, 0)',
            width: 0,
          },
        },
        textStyle: {
          color: `${baseTheme.variables?.['fgText']}`,
          fontSize: '20',
          fontWeight: 'normal',
        },
        position: 'top',
        backgroundColor: `${baseTheme.variables?.['bg']}`,
        borderColor: `${baseTheme.variables?.['border2']}`,
        borderWidth: 1,
        extraCssText: 'border-radius: 10px; padding: 8px 24px;',
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        offset: 5,
        data: [],
        axisTick: {
          show: false,
        },
        axisLabel: {
          color: `${baseTheme.variables?.['fg']}`,
          fontSize: '16',
        },
        axisLine: {
          lineStyle: {
            color: `${baseTheme.variables?.['border4']}`,
            width: 2,
          },
        },
      },
      yAxis: {
        type: 'value',
        axisLine: {
          lineStyle: {
            color: `${baseTheme.variables?.['border4']}`,
            width: 1,
          },
        },
        axisLabel: {
          color: `${baseTheme.variables?.['fg']}`,
          fontSize: '16',
        },
        axisTick: {
          show: false,
        },
        splitLine: {
          lineStyle: {
            color: `${baseTheme.variables?.['separator']}`,
            width: 1,
          },
        },
      },
      series: [
        this.getProfitLine(),
      ],
    };
  }

  updateChartOptions(ordersChartData: { chartLabel: string[], linesData: (number | null)[][] }) {
    const options = this.options;
    const series = this.getNewSeries(options.series, ordersChartData.linesData);
    const xAxis = this.getNewXAxis(options.xAxis, ordersChartData.chartLabel);

    this.options = {
      ...options,
      xAxis,
      series,
    };
  }

  getNewSeries(series: SeriesOption | SeriesOption[] | undefined, linesData: (number | null)[][]) {
    if (!(series instanceof Array))
      return;

    return series.map((line: object, index: number) => {
      return {
        ...line,
        data: linesData[index],
      };
    });
  }

  getNewXAxis(xAxis: EChartsOption["xAxis"], chartLabel: string[]) {
    return {
      ...xAxis,
      data: chartLabel,
    };
  }

  getProfitLine(): SeriesOption {
    return {
      type: 'line',
      smooth: true,
      symbolSize: 20,
      connectNulls: true,
      emphasis: {
        itemStyle: {
          color: '#ffffff',
          borderColor: `${baseTheme.variables?.['primary']}`,
          borderWidth: 2,
          opacity: 1
        }
      },
      itemStyle: {
        opacity: 0
      },
      lineStyle: {
        width: 4,
          type: 'solid',
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: `${baseTheme.variables?.['success']}`,
          }, {
            offset: 1,
            color: `${baseTheme.variables?.['successLight']}`,
          }])
      },
      areaStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: 'rgba(0, 214, 143, 0.2)',
        }, {
          offset: 1,
          color: 'rgba(44, 230, 155, 0.05)',
        }])
      },
      data: [],
    };
  }
}
