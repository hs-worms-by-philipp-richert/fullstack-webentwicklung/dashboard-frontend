import { Component, OnInit } from '@angular/core';
import { orderApi } from '../../../api/ordersApi/orders.api';
import { formatToCurrencyString } from 'src/app/utility/money.helper';
import { NbToastrService } from '@nebular/theme';
import { GenericRevenue } from 'src/app/shared/models/revenueTypes';

@Component({
  selector: 'ngx-profit-for-date',
  templateUrl: './profit-for-date.component.html',
  styleUrls: ['./profit-for-date.component.scss']
})
export class ProfitForDateComponent implements OnInit {
  constructor(private toastrService: NbToastrService) {
  }

  profit: string = formatToCurrencyString(0.00);
  todaysDate: Date = new Date()
  selectedDate: Date = new Date()
  showDay: String = '';
  showMonth: String = '';
  showYear: String = '';


  async ngOnInit() {
    await this.getProfitOnDate(this.todaysDate);
  }

  getSelectedDate($event: Date): void {
    this.selectedDate = $event;
    this.getProfitOnDate(this.selectedDate);
  }

  async getProfitOnDate(selectedDate: Date) {
    try {
      const res: GenericRevenue = await orderApi.getProfitOnDate(this.selectedDate);

      this.profit = formatToCurrencyString(res.revenue);

      this.showDay = ('0' + selectedDate.getDate()).slice(-2);
      this.showMonth = ('0' + (selectedDate.getMonth() + 1)).slice(-2);
      this.showYear = selectedDate.getFullYear().toString();
    } catch (error) {
      console.error('Error fetching selected date\'s profit', error);
      orderApi.errorHandler(this.toastrService, 'Error', 'Could not fetch selected date\'s profit');
    }
  }
}
