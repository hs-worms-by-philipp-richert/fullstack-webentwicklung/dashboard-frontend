import { Component, OnInit } from '@angular/core';
import { orderApi } from '../../../api/ordersApi/orders.api';
import { formatToCurrencyString } from 'src/app/utility/money.helper';
import { NbToastrService } from '@nebular/theme';
import { GenericRevenue } from 'src/app/shared/models/revenueTypes';

@Component({
  selector: 'ngx-revenue-for-date',
  templateUrl: './revenue-for-date.component.html',
  styleUrls: ['./revenue-for-date.component.scss']
}) 
export class RevenueForDateComponent implements OnInit {
  constructor(private toastrService: NbToastrService) {
  }

  revenue: string = formatToCurrencyString(0.00);
  selectedDate: Date = new Date();
  showDay: String = '';
  showMonth: String = '';
  showYear: String = '';

  async ngOnInit() {
    await this.getRevenueOnDate(this.selectedDate);
  }

  getSelectedDate($event: Date): void {
    this.selectedDate = $event;
    this.getRevenueOnDate(this.selectedDate);
  }

  async getRevenueOnDate(selectedDate: Date) {
    try {
      const res: GenericRevenue = await orderApi.getRevenueOnDate(this.selectedDate);

      this.revenue = formatToCurrencyString(res.revenue);

      this.showDay = ('0' + selectedDate.getDate()).slice(-2);
      this.showMonth = ('0' + (selectedDate.getMonth() + 1)).slice(-2);
      this.showYear = selectedDate.getFullYear().toString();
    } catch (error) {
      console.error('Error fetching revenue of that day', error);
      orderApi.errorHandler(this.toastrService, 'Error', 'Could not fetch the revenue of that day');
    }
  }
}
