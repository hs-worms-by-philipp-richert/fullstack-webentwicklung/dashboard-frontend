import { Component, AfterViewInit, OnChanges, Input } from "@angular/core";
import type { EChartsOption, SeriesOption } from 'echarts';
import { DEFAULT_THEME as baseTheme } from '@nebular/theme';
import * as echarts from 'echarts';

@Component({
  selector: 'ngx-revenue-chart',
  styleUrls: ['./revenue-chart.component.scss'],
  template: `<div echarts [options]="options" [merge]="options" class="echart" (chartInit)="onChartInit($event)"></div>`
})
export class RevenueChartComponent implements AfterViewInit, OnChanges {

  @Input()
  revenueChartData?: { chartLabel: string[], linesData: (number | null)[][] };

  echartsInstance: any;
  options: EChartsOption = {};

  ngOnChanges(): void {
    if (this.options && this.revenueChartData) {
      this.updateChartOptions(this.revenueChartData);
    }
  }

  onChartInit(echarts: any) {
    this.echartsInstance = echarts;
  }

  ngAfterViewInit(): void {
    this.setOptions();

    if (this.revenueChartData)
      this.updateChartOptions(this.revenueChartData);
  }

  setOptions() {
    this.options = {
      grid: {
        left: 100,
        top: 20,
        right: 0,
        bottom: 40,
      },
      tooltip: {
        trigger: 'item',
        axisPointer: {
          type: 'line',
          lineStyle: {
            color: 'rgba(0, 0, 0, 0)',
            width: 0,
          },
        },
        textStyle: {
          color: `${baseTheme.variables?.['fgText']}`,
          fontSize: '20',
          fontWeight: 'normal',
        },
        position: 'top',
        backgroundColor: `${baseTheme.variables?.['bg']}`,
        borderColor: `${baseTheme.variables?.['border2']}`,
        borderWidth: 1,
        extraCssText: 'border-radius: 10px; padding: 8px 24px;',
      },
      xAxis: {
        type: 'category',
        data: [],
        axisTick: {
          show: false,
        },
        axisLabel: {
          color: `${baseTheme.variables?.['fg']}`,
          fontSize: '16',
        },
        axisLine: {
          lineStyle: {
            color: `${baseTheme.variables?.['border4']}`,
            width: 2,
          },
        },
      },
      yAxis: {
        type: 'value',
        axisLine: {
          lineStyle: {
            color: `${baseTheme.variables?.['border4']}`,
            width: 1,
          },
        },
        axisLabel: {
          color: `${baseTheme.variables?.['fg']}`,
          fontSize: '16',
        },
        axisTick: {
          show: false,
        },
        splitLine: {
          lineStyle: {
            color: `${baseTheme.variables?.['separator']}`,
            width: 1,
          },
        },
      },
      series: [
        this.getRevenueBar(),
      ],
    };
  }

  updateChartOptions(ordersChartData: { chartLabel: string[], linesData: (number | null)[][] }) {
    const options = this.options;
    const series = this.getNewSeries(options.series, ordersChartData.linesData);
    const xAxis = this.getNewXAxis(options.xAxis, ordersChartData.chartLabel);

    this.options = {
      ...options,
      xAxis,
      series,
    };
  }

  getNewSeries(series: SeriesOption | SeriesOption[] | undefined, linesData: (number | null)[][]) {
    if (!(series instanceof Array))
      return;

    return series.map((line: object, index: number) => {
      return {
        ...line,
        data: linesData[index],
      };
    });
  }

  getNewXAxis(xAxis: EChartsOption["xAxis"], chartLabel: string[]) {
    return {
      ...xAxis,
      data: chartLabel,
    };
  }

  getRevenueBar(): SeriesOption {
    return {
      type: 'bar',
      itemStyle: {
        color: `${baseTheme.variables?.['primary']}`,
      },
      data: [],
    };
  }
}
