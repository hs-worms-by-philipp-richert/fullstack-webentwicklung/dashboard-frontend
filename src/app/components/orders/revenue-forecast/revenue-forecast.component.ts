import { Component, OnInit } from '@angular/core';
import { orderApi } from '../../../api/ordersApi/orders.api';
import { formatToCurrencyString } from 'src/app/utility/money.helper';
import { NbToastrService } from '@nebular/theme';
import { ForecastNextYearRevenue } from 'src/app/shared/models/revenueTypes';

@Component({
  selector: 'ngx-revenue-forecast',
  templateUrl: './revenue-forecast.component.html',
  styleUrls: ['./revenue-forecast.component.scss']
}) 
export class RevenueForecastComponent implements OnInit {
  constructor(private toastrService: NbToastrService) {
  }

  forecast: string = formatToCurrencyString(0.00);

  async ngOnInit() {
    await this.getRevenueForecastNextYear();
  }

  async getRevenueForecastNextYear() {
    try {
      const res: ForecastNextYearRevenue = await orderApi.getRevenueForecastNextYear();

      this.forecast = formatToCurrencyString(res.forecast);
    } catch (error) {
      console.error('Error fetching next year\'s forecast revenue', error);
      orderApi.errorHandler(this.toastrService, 'Error', 'Could not fetch next year\'s forecast revenue');
    }
  }
}
