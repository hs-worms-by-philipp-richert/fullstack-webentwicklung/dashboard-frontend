import { Component, OnInit } from '@angular/core';
import { customerApi } from '../../../api/customersApi/customer.api';
import { formatToCurrencyString } from 'src/app/utility/money.helper';
import { NbToastrService } from '@nebular/theme';
import { CustomerRevenueModel } from 'src/app/shared/models/customerRevenueModel';
import { CustomerOrderCountModel } from 'src/app/shared/models/customerOrderCountModel';

@Component({
  selector: 'ngx-top-customers',
  templateUrl: './top-customers.component.html',
  styleUrls: ['./top-customers.component.scss']
}) 
export class TopCustomersComponent implements OnInit {
  constructor(private toastrService: NbToastrService) {
  }

  customersWithMostRevenue: CustomerRevenueModel[] = [];
  customersWithMostOrders: CustomerOrderCountModel[] = [];

  async ngOnInit() {
    await this.getCustomersWithMostRevenue();
    await this.getCustomersWithMostOrders();
  }

  async getCustomersWithMostRevenue() {
    try {
      const res = await customerApi.getCustomersWithMostRevenue();

      this.customersWithMostRevenue = res.map(obj => {
        return { ...obj, revenue: formatToCurrencyString(Number(obj.revenue)) }
      });
    } catch (error) {
      console.error('Error fetching highest revenue customer', error);
      customerApi.errorHandler(this.toastrService, 'Error', 'Could not fetch highest revenue customer');
    }
  }

  async getCustomersWithMostOrders() {
    try {
      const res = await customerApi.getCustomersWithMostOrders();

      this.customersWithMostOrders = res;
    } catch (error) {
      console.error('Error fetching most orders customer', error);
      customerApi.errorHandler(this.toastrService, 'Error', 'Could not fetch most orders customer');
    }
  }
}
