import { Component, OnInit } from '@angular/core';
import { nationApi } from '../../../api/nationApi/nation.api';
import { NbToastrService } from '@nebular/theme';
import { NationTopOrderModel } from 'src/app/shared/models/nationTopOrderModel';

@Component({
  selector: 'ngx-most-orders-nation',
  templateUrl: './most-orders-nation.component.html',
  styleUrls: ['./most-orders-nation.component.scss']
}) 
export class MostOrdersNationComponent implements OnInit {
  constructor(private toastrService: NbToastrService) {
  }

  topOrdersNationName: string = '-';
  topOrdersNationCount: number = 0;

  async ngOnInit() {
    await this.getMostOrdersNation();
  }

  async getMostOrdersNation() {
    try {
      const res: NationTopOrderModel = await nationApi.getMostOrdersNation();

      this.topOrdersNationName = res.nationName.trim().toLocaleLowerCase();
      this.topOrdersNationCount = res.orderCount;

    } catch (error) {
      console.error('Error fetching nation with most orders', error);
      nationApi.errorHandler(this.toastrService, 'Error', 'Could not fetch nation with most orders');
    }
  }
}
