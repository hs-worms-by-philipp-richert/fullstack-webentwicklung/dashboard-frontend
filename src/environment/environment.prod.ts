export const environment = {
  production: true,
  baseApiHost: '',
  language: 'en-US',
  currency: 'USD'
};
