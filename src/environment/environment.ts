export const environment = {
  production: false,
  baseApiHost: 'http://localhost:3000',
  language: 'en-US',
  currency: 'USD'
};
